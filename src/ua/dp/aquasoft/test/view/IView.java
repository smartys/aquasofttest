package ua.dp.aquasoft.test.view;

import ua.dp.aquasoft.test.presenter.IPresenter;

/**
 * Represents interface for view in MVP pattern.
 * <p>Can be implemented in different views.
 * @author Sergii Martynenko
 */
public interface IView {

    /**
     * Sets a presenter for a view
     * @param presenter presenter
     */
    void setPresenter(IPresenter presenter);

    /**
     * Gets a presenter in bounds of MVP pattern
     * @return presenter
     */
    IPresenter getPresenter();
}
