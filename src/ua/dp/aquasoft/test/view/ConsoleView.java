package ua.dp.aquasoft.test.view;

import ua.dp.aquasoft.test.presenter.IPresenter;

/**
 * Represents console view for output information
 * @author Sergii Martynenko
 */
public class ConsoleView implements IView {

    /**
     * Link to the presenter for communication in
     * bounds of a pattern.
     */
    private IPresenter spreadSheetPresenter;


    @Override
    public void setPresenter(IPresenter presenter) {
        spreadSheetPresenter = presenter;
    }

    @Override
    public IPresenter getPresenter() {
        return spreadSheetPresenter;
    }
}
