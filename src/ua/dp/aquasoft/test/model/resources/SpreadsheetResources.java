package ua.dp.aquasoft.test.model.resources;

/**
 * Represents strings for spreadsheet simulator
 * @author Sergii Martynenko
 */
public interface SpreadsheetResources {
    int LITERA_A = 65;
    String TAB = "\\t";
    String WELCOME_STRING = "Welcome to spreadsheet simulator.";
    String ENTER_STRING = "Please, enter input data:";
    String FILE_READ_STRING = "*** file reading ***";
    String SPREADSHEET_STRING = "\n**** Spreadsheet ****";
    String END_STRING = "\n********************";

    // Error strings
    String ERROR_STRING = "#ERROR";
    String ERRORS_STRING = "Errors are: ";
    String ERROR_SOURCE_STRING = "There are some problems with data source";
    String ERROR_NUMBER_STRING = "Please, enter 2 numbers for rows and columns";
    String ERROR_NEGATIVE_STRING = "Incorrect input, numbers should be positive";
    String ERROR_ZERO_STRING = "Incorrect input, at least 2 numbers are needed";
    String ERROR_NO_DATA_STRING = "Incorrect input, need more information";
    String ERROR_ANALYZE = "Expression in a cell is incorrect";
    String ERROR_EVALUATE_TYPE = "No such type for evaluation";
    String ERROR_EVALUATE_OPERATION = "No such operation";
    String ERROR_EVALUATE_FORMAT = "Operands must be numbers";
    String ERROR_BY_ZERO = "Division by zero";
    String ERROR_OUT_BOUNDS = "Such cell doesn't exist";


}
