package ua.dp.aquasoft.test.model.token;

import ua.dp.aquasoft.test.model.exceptions.SpreadsheetAnalyzeException;
import ua.dp.aquasoft.test.model.exceptions.SpreadsheetEvaluationException;

/**
 * Represents analyzer of tokens
 * @author  Sergii Martynenko
 */
public interface ITokenAnalyzer {

    enum TokenType {EXPRESSION, NUMBER, TEXT, REFERENCE, OPERATION, TERM, EMPTY}

    /**
     * Analyzes formula string to exclude tokens
     * @param formula string representation of a formula
     * @return token with list of tokens
     * @throws SpreadsheetAnalyzeException when any error occurs
     */
    AbstractToken analyze(String formula) throws SpreadsheetAnalyzeException;

    /**
     * Evaluates token
     * @param tokenForEvaluation token for evaluation
     * @return string representation of result
     * @throws SpreadsheetEvaluationException when any error occurs
     */
    String evaluate(AbstractToken tokenForEvaluation) throws SpreadsheetEvaluationException;
}
