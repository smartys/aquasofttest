package ua.dp.aquasoft.test.model.token;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Represents text
 * @author Sergii Martynenko
 */
public class TextToken extends AbstractToken {
    public static final String TEXT_REG_EXP = "\'.+";

    public TextToken() {
        tokenType = ITokenAnalyzer.TokenType.TEXT;
        regExpPattern = Pattern.compile(TEXT_REG_EXP);
    }

    @Override
    public boolean isFitToString(String formula) {
        regExpMatcher = regExpPattern.matcher(formula);
        return regExpMatcher.matches();
    }

    @Override
    public List<AbstractToken> convertStringToTokens(String formula) {
        List<AbstractToken> textList = new ArrayList<AbstractToken>();
        setValueAsString(formula.substring(1));
        textList.add(this);
        return textList;
    }
}
