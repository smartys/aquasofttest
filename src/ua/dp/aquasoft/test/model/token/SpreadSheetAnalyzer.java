package ua.dp.aquasoft.test.model.token;

import ua.dp.aquasoft.test.model.exceptions.SpreadsheetAnalyzeException;
import ua.dp.aquasoft.test.model.exceptions.SpreadsheetEvaluationException;
import ua.dp.aquasoft.test.model.resources.SpreadsheetResources;
import ua.dp.aquasoft.test.model.spreadsheet.ISpreadSheetModel;
import ua.dp.aquasoft.test.model.spreadsheet.SpreadsheetCell;

import java.util.*;

/**
 * Represents spread sheet analyzer
 * @author  Sergii Martynenko
 */
public class SpreadSheetAnalyzer implements ITokenAnalyzer {
    /**
     * Possible tokens
     */
    private List<AbstractToken> possibleValuesList;
    /**
     * Link to the model, which uses analyzer
     */
    private ISpreadSheetModel spreadSheetModel;

    public SpreadSheetAnalyzer(ISpreadSheetModel model) {
        possibleValuesList = new ArrayList<AbstractToken>();
        possibleValuesList.add(new ExpressionToken());
        possibleValuesList.add(new TextToken());
        possibleValuesList.add(new NonNegativeNumberToken());
        possibleValuesList.add(new EmptyToken());
        spreadSheetModel = model;
    }

    @Override
    public AbstractToken analyze(String formula) throws SpreadsheetAnalyzeException {
        AbstractToken tokenWithList = null;
        Iterator<AbstractToken> tokenIterator = possibleValuesList.iterator();
        boolean isCheckCompleted = false;
        while (tokenIterator.hasNext() && !isCheckCompleted) {
            AbstractToken possibleValue = tokenIterator.next();
            if (possibleValue.isFitToString(formula)) {
                tokenWithList = TokenFactory.build(possibleValue.getTokenType());
                tokenWithList.setTokenList(tokenWithList.convertStringToTokens(formula));
                isCheckCompleted = true;
            }
        }
        if (!isCheckCompleted) {
            throw new SpreadsheetAnalyzeException(SpreadsheetResources.ERROR_ANALYZE);
        }
        return tokenWithList;
    }

    @Override
    public String evaluate(AbstractToken tokenForEvaluation) throws SpreadsheetEvaluationException {
        String result = null;
        switch (tokenForEvaluation.getTokenType()) {
            case EXPRESSION: {
                result = Integer.toString(evaluatePostfix(tokenForEvaluation));
                break;
            }
            case NUMBER: {
                result = tokenForEvaluation.getTokenList().get(0).getValueAsString();
                break;
            }
            case TEXT: {
                result = tokenForEvaluation.getTokenList().get(0).getValueAsString();
                break;
            }
            case EMPTY: {
                result = tokenForEvaluation.getTokenList().get(0).getValueAsString();
                break;
            }
            default: {
                throw new SpreadsheetEvaluationException(SpreadsheetResources.ERROR_EVALUATE_TYPE);
            }
        }
        return result;
    }

    private int evaluatePostfix(AbstractToken tokenForEvaluation) throws SpreadsheetEvaluationException {
        Stack<Integer> evaluationStack = new Stack<Integer>();
        for (AbstractToken currentToken : tokenForEvaluation.getTokenList()) {
            try {
                if (!currentToken.getTokenType().equals(TokenType.OPERATION)) {
                    int tokenValue;
                    if (currentToken.getTokenType().equals(TokenType.REFERENCE)) {
                        SpreadsheetCell referenceCell = spreadSheetModel.getCellByIndex(Character.toString(currentToken.getValueAsString().charAt(1)), Character.toString(currentToken.getValueAsString().charAt(0)));
                        tokenValue = Integer.parseInt((referenceCell.getCellToken().getTokenType().equals(TokenType.EMPTY))? EmptyToken.EMPTY_AS_NUMBER : evaluate(referenceCell.getCellToken()));
                    } else if (currentToken.getTokenType().equals(TokenType.EMPTY)) {
                        tokenValue = 0;
                    } else {
                        tokenValue = Integer.parseInt(currentToken.getValueAsString());
                    }
                    evaluationStack.push(tokenValue);
                } else {
                    evaluationStack.push(operate(currentToken.getValueAsString(), evaluationStack.pop(), evaluationStack.pop()));
                }
            } catch (NumberFormatException nfe) {
                throw new SpreadsheetEvaluationException(SpreadsheetResources.ERROR_EVALUATE_FORMAT);
            } catch (ArithmeticException ae) {
                throw new SpreadsheetEvaluationException(SpreadsheetResources.ERROR_BY_ZERO);
            } catch (IndexOutOfBoundsException ioe) {
                throw new SpreadsheetEvaluationException(SpreadsheetResources.ERROR_OUT_BOUNDS);
            }

        }
        return evaluationStack.pop();
    }

    private int operate(String operation, int op1, int op2) {
        if(operation.equals("+")) {
            return op1 + op2;
        }
        else if(operation.equals("-")) {
            return op2 - op1;
        }
        else if(operation.equals("*")) {
            return op1 * op2;
        }
        else if(operation.equals("/")) {
            return op2 / op1;
        } else {
            throw new SpreadsheetEvaluationException(SpreadsheetResources.ERROR_EVALUATE_OPERATION);
        }

    }


}
