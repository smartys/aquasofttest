package ua.dp.aquasoft.test.model.token;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Represents non negative number
 * @author Sergii Martynenko
 */
public class NonNegativeNumberToken extends AbstractToken {
    public static final String NUMBER_REG_EXP = "\\d+";

    public NonNegativeNumberToken() {
        tokenType = ITokenAnalyzer.TokenType.NUMBER;
        regExpPattern = Pattern.compile(NUMBER_REG_EXP);
    }

    @Override
    public boolean isFitToString(String formula) {
        regExpMatcher = regExpPattern.matcher(formula);
        return regExpMatcher.matches();
    }

    @Override
    public List<AbstractToken> convertStringToTokens(String formula) {
        List<AbstractToken> numberTokenList = new ArrayList<AbstractToken>();
        setValueAsString(formula);
        numberTokenList.add(this);
        return numberTokenList;
    }
}
