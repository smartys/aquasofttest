package ua.dp.aquasoft.test.model.token;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Represents cell reference
 * @author Sergii Martynenko
 */
public class CellReferenceToken extends AbstractToken {
    public static final String REFERENCE_REG_EXP = "[a-zA-Z]+\\d+";

    public CellReferenceToken() {
        tokenType = ITokenAnalyzer.TokenType.REFERENCE;
        regExpPattern = Pattern.compile(REFERENCE_REG_EXP);
    }

    @Override
    public boolean isFitToString(String formula) {
        regExpMatcher = regExpPattern.matcher(formula);
        return regExpMatcher.matches();
    }

    @Override
    public List<AbstractToken> convertStringToTokens(String formula) {
        List<AbstractToken> referenceList = new ArrayList<AbstractToken>();
        setValueAsString(formula);
        referenceList.add(this);
        return referenceList;
    }
}
