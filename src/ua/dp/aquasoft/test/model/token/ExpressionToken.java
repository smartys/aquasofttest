package ua.dp.aquasoft.test.model.token;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Pattern;

/**
 * Represents expression
 *
 * @author Sergii Martynenko
 */
public class ExpressionToken extends AbstractToken {
    public static final String EXPRESSION_REG_EXP = "=(([a-zA-Z]+\\d+)|\\d+)((\\+|-|/|\\*)(([a-zA-Z]+\\d+)|\\d+))*";

    public ExpressionToken() {
        tokenType = ITokenAnalyzer.TokenType.EXPRESSION;
        regExpPattern = Pattern.compile(EXPRESSION_REG_EXP);
    }

    @Override
    public boolean isFitToString(String formula) {
        regExpMatcher = regExpPattern.matcher(formula);
        return regExpMatcher.matches();
    }

    @Override
    public List<AbstractToken> convertStringToTokens(String formula) {
        List<AbstractToken> infixList = new ArrayList<AbstractToken>();
        String stringWithoutEqual = formula.substring(1);
        String[] tokenAsStringArray = stringWithoutEqual.split(OperationToken.OPERATION_REG_EXP);
        StringBuilder currentTokenStringBuilder = new StringBuilder();
        int currentIndex = 0;
        while (currentIndex < stringWithoutEqual.length()) {
            currentTokenStringBuilder.append(Character.toString(stringWithoutEqual.charAt(currentIndex)));
            boolean isTermToken = false, isOperationToken = false;
            for (int indexToken = 0; indexToken < tokenAsStringArray.length && !isTermToken; indexToken++) {
                if (currentTokenStringBuilder.toString().length() == tokenAsStringArray[indexToken].length()
                        && currentTokenStringBuilder.toString().equals(tokenAsStringArray[indexToken])) {
                    isTermToken = true;
                    TermToken termToken = new TermToken();
                    infixList.add(termToken.convertStringToTokens(currentTokenStringBuilder.toString()).get(0));
                }
            }
            if (!isTermToken) {
                OperationToken operationToken = new OperationToken();
                if (operationToken.isFitToString(currentTokenStringBuilder.toString())) {
                    isOperationToken = true;
                    infixList.add(operationToken.convertStringToTokens(currentTokenStringBuilder.toString()).get(0));
                }
            }
            if (isTermToken || isOperationToken) {
                currentTokenStringBuilder.setLength(0);
            }
            currentIndex++;
        }
        return infixToPostfix(infixList);
    }

    /**
     * Helper method wich converts infix to postfix notation
     * @param infixList list with token in infix notation
     * @return list with tokens in postfix notation
     */
    private List<AbstractToken> infixToPostfix(List<AbstractToken> infixList) {
        List<AbstractToken> postfixList = new ArrayList<AbstractToken>();
        Stack<AbstractToken> operationStack = new Stack<AbstractToken>();
        for (int indexList = 0; indexList < infixList.size(); indexList++) {
            AbstractToken currentToken = infixList.get(indexList);
            if (currentToken.getTokenType().equals(ITokenAnalyzer.TokenType.REFERENCE)
                    || currentToken.getTokenType().equals(ITokenAnalyzer.TokenType.NUMBER)) {
                postfixList.add(currentToken);
            } else {
                if (operationStack.empty()) {
                    operationStack.push(currentToken);
                } else {
                    if (OperationToken.getPriority(currentToken.getValueAsString()) > OperationToken.getPriority(operationStack.peek().getValueAsString())) {
                        operationStack.push(currentToken);
                    } else {
                        postfixList.add(operationStack.pop());
                        operationStack.push(currentToken);
                    }
                }
            }

        }
        while (!operationStack.empty()) {
            postfixList.add(operationStack.pop());
        }
        return postfixList;
    }
}
