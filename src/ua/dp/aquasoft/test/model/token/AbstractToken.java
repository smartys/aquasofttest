package ua.dp.aquasoft.test.model.token;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ua.dp.aquasoft.test.model.token.ITokenAnalyzer.*;

/**
 * Represents abstract token
 * @author Sergii Martynenko
 */
public abstract class AbstractToken {
    /**
     * Pattern for regular expression
     */
    protected Pattern regExpPattern;
    /**
     * Matcher for regular expression
     */
    protected Matcher regExpMatcher;
    /**
     * String representation of a token value
     */
    protected String valueAsString;
    /**
     * List with tokens
     */
    protected List<AbstractToken> tokenList;
    /**
     * Type of the token
     */
    protected TokenType tokenType;

    /**
     * Checks is string fits to token
     * @param formula formula of cell
     * @return true - if firs, false - otherwise
     */
    public abstract boolean isFitToString(String formula);

    /**
     * Converts formula string to list with a concrete token
     * @param formula formula string
     * @return list with tokens
     */
    public abstract List<AbstractToken> convertStringToTokens(String formula);

    /**
     * Returns string value of a token
     * @return string value
     */
    public String getValueAsString() {
        return valueAsString;
    }

    /**
     * Sets string value of a token
     * @param valueAsString string value
     */
    public void setValueAsString(String valueAsString) {
        this.valueAsString = valueAsString;
    }

    /**
     * Returns token list
     * @return token list
     */
    public List<AbstractToken> getTokenList() {
        return tokenList;
    }

    /**
     * Set a token list for token
     * @param tokenList token list
     */
    public void setTokenList(List<AbstractToken> tokenList) {
        this.tokenList = tokenList;
    }

    /**
     * Returns token type
     * @return token type
     */
    public TokenType getTokenType() {
        return tokenType;
    }
}
