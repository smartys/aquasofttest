package ua.dp.aquasoft.test.model.token;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Martynenko on 13.09.2015.
 */
public class OperationToken extends AbstractToken {
    public static final String OPERATION_REG_EXP = "\\+|-|/|\\*";

    public OperationToken() {
        tokenType = ITokenAnalyzer.TokenType.OPERATION;
        regExpPattern = Pattern.compile(OPERATION_REG_EXP);
    }

    @Override
    public boolean isFitToString(String formula) {
        regExpMatcher = regExpPattern.matcher(formula);
        return regExpMatcher.matches();
    }

    @Override
    public List<AbstractToken> convertStringToTokens(String formula) {
        List<AbstractToken> operationList = new ArrayList<AbstractToken>();
        setValueAsString(formula);
        operationList.add(this);
        return operationList;
    }

    /**
     * Returns the priority of operations
     * @param operation operation
     * @return priority (2 - for / and *, 1 - for + and -)
     */
    public static int getPriority(String operation) {
        if (operation.equals("*") || operation.equals("/")) {
            return 2;
        } else {
            return 1;
        }
    }
}
