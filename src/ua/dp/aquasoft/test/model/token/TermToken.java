package ua.dp.aquasoft.test.model.token;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Represents terms
 * @author Sergii Martynenko
 */
public class TermToken extends AbstractToken {
    public static final String TERM_REG_EXP = "([a-zA-Z]+\\d+)|\\d+";

    public TermToken() {
        tokenType = ITokenAnalyzer.TokenType.TERM;
        regExpPattern = Pattern.compile(TERM_REG_EXP);
    }

    @Override
    public boolean isFitToString(String formula) {
        regExpMatcher = regExpPattern.matcher(formula);
        return regExpMatcher.matches();
    }

    @Override
    public List<AbstractToken> convertStringToTokens(String formula) {
        List<AbstractToken> returnList = null;
        NonNegativeNumberToken numberToken = new NonNegativeNumberToken();
        CellReferenceToken referenceToken = new CellReferenceToken();
        if (numberToken.isFitToString(formula)) {
            returnList = numberToken.convertStringToTokens(formula);
        } else if (referenceToken.isFitToString(formula)) {
            returnList = referenceToken.convertStringToTokens(formula);
        }
        return returnList;
    }

}
