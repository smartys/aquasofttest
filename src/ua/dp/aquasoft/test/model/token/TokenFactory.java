package ua.dp.aquasoft.test.model.token;

/**
 * Represents a factory of tokens depending on them type
 * @author  Sergii Martynenko
 */
public class TokenFactory {

    /**
     * Builds a token in accordance with the token type
     * @param tokenType token type
     * @return token
     */
    public static AbstractToken build(ITokenAnalyzer.TokenType tokenType) {
        AbstractToken tokenToReturn = null;
        if (tokenType.equals(ITokenAnalyzer.TokenType.EXPRESSION)) {
            tokenToReturn = new ExpressionToken();
        } else if (tokenType.equals(ITokenAnalyzer.TokenType.NUMBER)) {
            tokenToReturn = new NonNegativeNumberToken();
        } else if (tokenType.equals(ITokenAnalyzer.TokenType.TEXT)) {
            tokenToReturn = new TextToken();
        } else if (tokenType.equals(ITokenAnalyzer.TokenType.EMPTY)) {
            tokenToReturn = new EmptyToken();
        } else if (tokenType.equals(ITokenAnalyzer.TokenType.TERM)) {
            tokenToReturn = new TermToken();
        } else if (tokenType.equals(ITokenAnalyzer.TokenType.OPERATION)) {
            tokenToReturn = new OperationToken();
        } else if (tokenType.equals(ITokenAnalyzer.TokenType.REFERENCE)) {
            tokenToReturn = new CellReferenceToken();
        }
        return tokenToReturn;
    }
}
