package ua.dp.aquasoft.test.model.token;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Represents empty token
 * @author Sergii Martynenko
 */
public class EmptyToken extends AbstractToken{
    public static final String EMPTY_AS_NUMBER = "0";
    public static final String EMPTY_REG_EXP = "^$";

    public EmptyToken() {
        tokenType = ITokenAnalyzer.TokenType.EMPTY;
        regExpPattern = Pattern.compile(EMPTY_REG_EXP);
    }

    @Override
    public boolean isFitToString(String formula) {
        regExpMatcher = regExpPattern.matcher(formula);
        return regExpMatcher.matches();
    }

    @Override
    public List<AbstractToken> convertStringToTokens(String formula) {
        List<AbstractToken> emptyTokenList = new ArrayList<AbstractToken>();
        setValueAsString(formula);
        emptyTokenList.add(this);
        return emptyTokenList;
    }
}
