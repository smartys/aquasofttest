package ua.dp.aquasoft.test.model.spreadsheet;

/**
 * Represents model in MVP pattern
 * @author Sergii Martynenko
 */
public interface ISpreadSheetModel {

    /**
     * Inits a spread sheet
     * @param rows number of rows
     * @param cols number of columns
     */
    void initSpreadSheet(int rows, int cols);

    /**
     * Gets spread sheet cell by string index
     * @param rowIndex row index as a number
     * @param colIndex column index as a litera
     * @return cell with pointed index
     */
    SpreadsheetCell getCellByIndex(String rowIndex, String colIndex);

    /**
     * Returns spread sheet cell by integer index
     * @param rowIndex row index
     * @param colIndex column index
     * @return cell[rowIndex][colIndex]
     */
    SpreadsheetCell getCellByIndex(int rowIndex, int colIndex);

    /**
     * Returns amount of rows
     * @return amount of rows
     */
    int getSpreadSheetRowsNumber();

    /**
     * Returns amount of columns
     * @return amount of columns
     */
    int getSpreadSheetColumnsNumber();

    /**
     * Makes lexical analysis if needed
     */
    void doAnalysis();
}
