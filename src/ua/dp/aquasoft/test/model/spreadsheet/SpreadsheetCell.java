package ua.dp.aquasoft.test.model.spreadsheet;

import ua.dp.aquasoft.test.model.resources.SpreadsheetResources;
import ua.dp.aquasoft.test.model.token.AbstractToken;

/**
 * Represents spreadsheet cell
 * @author  Sergii Martynenko
 */
public class SpreadsheetCell {
    /**
     * Value of the cell
     */
    private String resultValue;
    /**
     * Formula of the cell
     */
    private String formula;
    /**
     * Row index of the cell
     */
    private int row;
    /**
     * Column cell index
     */
    private int column;
    /**
     * Combine cell index (column + row)
     */
    private String cellIndex;
    /**
     * Indicates is error exists in this cell
     */
    private boolean isError = false;
    /**
     * Token of the cell
     */
    private AbstractToken cellToken;
    /**
     * Error message with description
     */
    private String errorMessage;

    public SpreadsheetCell(int aRow, int aCol) {
        row = aRow;
        column = aCol;
        cellIndex = Character.toString((char)(aCol + SpreadsheetResources.LITERA_A)) + Integer.toString(aRow + 1);
    }
    /**
     * Gets cell index row
     * @return index row
     */
    public int getRow() {
        return row;
    }

    /**
     * Gets cell index column
     * @return index column
     */
    public int getColumn() {
        return column;
    }

    /**
     * Gets combine cell index
     * @return column+row index
     */
    public String getCellIndex() {
        return cellIndex;
    }

    /**
     * Gets cell value
     * @return cell value
     */
    public String getValue() {
        return resultValue;
    }

    /**
     * Sets cell value
     * @param value cell value
     */
    public void setValue(String value) {
        this.resultValue = value;
    }

    @Override
    public String toString() {
        return getCellIndex();
    }

    /**
     * Gets string formula of the cell
     * @return formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * Sets formula of the cell
     * @param formula string formula
     */
    public void setFormula(String formula) {
        this.formula = formula;
    }

    /**
     * Returns error existing for current cell
     * @return true - if error exists, false - otherwise
     */
    public boolean isError() {
        return isError;
    }

    /**
     * Sets error state
     */
    public void setErrorState() {
        isError = true;
    }

    /**
     * Returns cell token
     * @return cell token
     */
    public AbstractToken getCellToken() {
        return cellToken;
    }

    /**
     * Set the cell token
     * @param cellToken token to set
     */
    public void setCellToken(AbstractToken cellToken) {
        this.cellToken = cellToken;
    }

    /**
     * Returns error message for cell
     * @return error message
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets error message
     * @param errorMessage error message
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
