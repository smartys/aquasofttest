package ua.dp.aquasoft.test.model.spreadsheet;

import ua.dp.aquasoft.test.model.exceptions.SpreadsheetAnalyzeException;
import ua.dp.aquasoft.test.model.exceptions.SpreadsheetEvaluationException;
import ua.dp.aquasoft.test.model.resources.SpreadsheetResources;
import ua.dp.aquasoft.test.model.token.ITokenAnalyzer;
import ua.dp.aquasoft.test.model.token.SpreadSheetAnalyzer;

/**
 * Represents spreadsheet
 * @author Sergii Martynenko
 */
public class SpreadSheet implements ISpreadSheetModel {
    /**
     * Number of rows
     */
    private int rowNumber;
    /**
     * Number of columns
     */
    private int colNumber;
    /**
     * Spreadsheet
     */
    private SpreadsheetCell[][] spreadSheet;
    /**
     * Analyzer for tokens
     */
    private ITokenAnalyzer tokenAnalyzer;

    @Override
    public void initSpreadSheet(int rows, int cols) {
        rowNumber = rows;
        colNumber = cols;
        spreadSheet = new SpreadsheetCell[rowNumber][colNumber];
        for (int i = 0; i < rowNumber; i++)
            for (int j = 0; j < colNumber; j++)
                spreadSheet[i][j] = new SpreadsheetCell(i, j);
        tokenAnalyzer = new SpreadSheetAnalyzer(this);
    }

    @Override
    public SpreadsheetCell getCellByIndex(String rowIndex, String colIndex) {
        return spreadSheet[Integer.parseInt(rowIndex) - 1][colIndex.charAt(0) - SpreadsheetResources.LITERA_A];
    }

    @Override
    public SpreadsheetCell getCellByIndex(int rowIndex, int colIndex) {
        return spreadSheet[rowIndex][colIndex];
    }

    @Override
    public int getSpreadSheetRowsNumber() {
        return rowNumber;
    }

    @Override
    public int getSpreadSheetColumnsNumber() {
        return colNumber;
    }

    @Override
    public void doAnalysis() {
        for (int rowIndex = 0; rowIndex < rowNumber; rowIndex++)
            for (int colIndex = 0; colIndex < colNumber; colIndex++) {
                try {
                    getCellByIndex(rowIndex, colIndex).setCellToken(tokenAnalyzer.analyze(getCellByIndex(rowIndex, colIndex).getFormula()));
                } catch (SpreadsheetAnalyzeException sae) {
                    getCellByIndex(rowIndex, colIndex).setErrorState();
                    getCellByIndex(rowIndex, colIndex).setValue(SpreadsheetResources.ERROR_STRING);
                    getCellByIndex(rowIndex, colIndex).setErrorMessage(sae.getMessage());
                }

            }
        for (int rowIndex = 0; rowIndex < rowNumber; rowIndex++)
            for (int colIndex = 0; colIndex < colNumber; colIndex++) {
                try {
                    if (!getCellByIndex(rowIndex, colIndex).isError()) {
                        getCellByIndex(rowIndex, colIndex).setValue(tokenAnalyzer.evaluate(getCellByIndex(rowIndex, colIndex).getCellToken()));
                    }
                } catch (SpreadsheetEvaluationException see) {
                    getCellByIndex(rowIndex, colIndex).setErrorState();
                    getCellByIndex(rowIndex, colIndex).setValue(SpreadsheetResources.ERROR_STRING);
                    getCellByIndex(rowIndex, colIndex).setErrorMessage(see.getMessage());
                }
            }
    }
}
