package ua.dp.aquasoft.test.model.exceptions;

/**
 * Represents exception, when formuls doesn't correspond to any token
 * @author  Sergii Martynenko
 */
public class SpreadsheetAnalyzeException extends RuntimeException {
    public SpreadsheetAnalyzeException() { super(); }
    public SpreadsheetAnalyzeException(String message) { super(message); }
    public SpreadsheetAnalyzeException(String message, Throwable cause) { super(message, cause); }
    public SpreadsheetAnalyzeException(Throwable cause) { super(cause); }
}
