package ua.dp.aquasoft.test.model.exceptions;

/**
 * Represents exception for case, when user enters negative number of rows or columns
 * @author Sergii Martynenko
 */
public class SpreadsheetDataInputException extends RuntimeException {
    public SpreadsheetDataInputException() { super(); }
    public SpreadsheetDataInputException(String message) { super(message); }
    public SpreadsheetDataInputException(String message, Throwable cause) { super(message, cause); }
    public SpreadsheetDataInputException(Throwable cause) { super(cause); }
}
