package ua.dp.aquasoft.test.model.exceptions;

/**
 * Represents evaluation exception
 * @author Sergii Martynenko
 */
public class SpreadsheetEvaluationException extends RuntimeException {
    public SpreadsheetEvaluationException() {
        super();
    }

    public SpreadsheetEvaluationException(String message) {
        super(message);
    }

    public SpreadsheetEvaluationException(String message, Throwable cause) {
        super(message, cause);
    }

    public SpreadsheetEvaluationException(Throwable cause) {
        super(cause);
    }
}
