package ua.dp.aquasoft.test;

import ua.dp.aquasoft.test.model.spreadsheet.ISpreadSheetModel;
import ua.dp.aquasoft.test.model.spreadsheet.SpreadSheet;
import ua.dp.aquasoft.test.presenter.IPresenter;
import ua.dp.aquasoft.test.presenter.SpreadSheetPresenter;
import ua.dp.aquasoft.test.view.ConsoleView;
import ua.dp.aquasoft.test.view.IView;

/**
 * Program entry point
 * User can provide input data through keyboard input
 * or with paramater for program.
 * @author Sergii Martynenko
 */
public class Main {

    public static void main(String[] args) {
        IView consoleView = new ConsoleView();
        ISpreadSheetModel spreadSheetModel = new SpreadSheet();
        IPresenter spreadSheetPresenter = new SpreadSheetPresenter();
        spreadSheetPresenter.setModel(spreadSheetModel);
        spreadSheetPresenter.setView(consoleView);
        consoleView.setPresenter(spreadSheetPresenter);
        spreadSheetPresenter.inputData(args);
        spreadSheetPresenter.analyzeData();


    }


}
