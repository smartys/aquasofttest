package ua.dp.aquasoft.test.presenter;

import ua.dp.aquasoft.test.model.spreadsheet.ISpreadSheetModel;
import ua.dp.aquasoft.test.view.IView;

/**
 * Represents presenter part in MVP pattern
 * @author Sergii Martynenko
 */
public interface IPresenter {

    /**
     * Sets a view to presenter
     * @param spreadSheetView current view
     */
    void setView(IView spreadSheetView);

    /**
     * Returns current view
     * @return current view
     */
    IView getView();

    /**
     * Sets a spreadsheet model to a presenter
     * @param model current model
     */
    void setModel(ISpreadSheetModel model);

    /**
     * Gets a current spreadsheet model
     * @return current model
     */
    ISpreadSheetModel getModel();

    /**
     * Reads user's input
     * @param programParameters parameters of a program (first file name of input data, if exists)
     */
    void inputData(String[] programParameters);

    /**
     * Analyzes input data and make syntax analysis
     */
    void analyzeData();
}
