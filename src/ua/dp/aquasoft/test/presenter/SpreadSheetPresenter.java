package ua.dp.aquasoft.test.presenter;

import ua.dp.aquasoft.test.model.exceptions.SpreadsheetDataInputException;
import ua.dp.aquasoft.test.model.resources.SpreadsheetResources;
import ua.dp.aquasoft.test.model.spreadsheet.ISpreadSheetModel;
import ua.dp.aquasoft.test.view.IView;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Represents presenter for spread sheet
 * @author Sergii Martynenko
 */
public class SpreadSheetPresenter implements IPresenter {
    private ISpreadSheetModel model;
    private IView view;


    @Override
    public void setView(IView spreadSheetView) {
        view = spreadSheetView;
    }

    @Override
    public IView getView() {
        return view;
    }

    @Override
    public void setModel(ISpreadSheetModel model) {
        this.model = model;
    }

    @Override
    public ISpreadSheetModel getModel() {
        return model;
    }

    @Override
    public void inputData(String[] programParameters) {
        BufferedReader bufferedReader = null;
        output(SpreadsheetResources.WELCOME_STRING);
        try {

            if (programParameters.length > 0 && programParameters[0] != null) {
                bufferedReader = new BufferedReader(new FileReader(programParameters[0]));
                output(SpreadsheetResources.FILE_READ_STRING);
            } else {
                output(SpreadsheetResources.ENTER_STRING);
                bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            }
            readInput(bufferedReader);
        } catch (IOException e) {
            output(SpreadsheetResources.ERROR_SOURCE_STRING);
        } catch (NumberFormatException nfe) {
            output(SpreadsheetResources.ERROR_NUMBER_STRING);
        } catch (SpreadsheetDataInputException sdie) {
            output(sdie.getMessage());
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    output(SpreadsheetResources.ERROR_SOURCE_STRING);
                }
            }
        }
    }

    @Override
    public void analyzeData() {
        model.doAnalysis();
        output(model);
    }

    /**
     * Helper method for reading input data
     *
     * @param inputReader input reader
     */
    private void readInput(BufferedReader inputReader)
            throws IOException, NumberFormatException, SpreadsheetDataInputException {
        String line = inputReader.readLine();
        String[] values = line.split(SpreadsheetResources.TAB);
        if (values.length > 1) {
            int rows = Integer.parseInt(values[0]);
            int columns = Integer.parseInt(values[1]);
            if (rows > 0 && columns > 0) {
                model.initSpreadSheet(rows, columns);
                int rowIndex = 0;
                do {
                    line = inputReader.readLine();
                    if (line == null) {
                        throw new SpreadsheetDataInputException(SpreadsheetResources.ERROR_NO_DATA_STRING);
                    }
                    values = line.split(SpreadsheetResources.TAB);
                    if (values.length != columns) {
                        throw new SpreadsheetDataInputException(SpreadsheetResources.ERROR_NO_DATA_STRING);
                    }
                    for (int colIndex = 0; colIndex < columns; colIndex++) {
                        model.getCellByIndex(rowIndex, colIndex).setFormula(values[colIndex]);
                    }
                    rowIndex++;
                } while (rowIndex < rows);
            } else {
                throw new SpreadsheetDataInputException(SpreadsheetResources.ERROR_NEGATIVE_STRING);
            }
        } else {
            throw new SpreadsheetDataInputException(SpreadsheetResources.ERROR_ZERO_STRING);
        }
    }

    /**
     * Helper method to avoid System.out.println
     *
     * @param stringForOutput string for output
     */
    private void output(String stringForOutput) {
        System.out.println(stringForOutput);
    }

    /**
     * Helper method to output spreadsheet
     * @param model model for output
     */
    private void output(ISpreadSheetModel model) {
        boolean isErrorHappen = false;
        output(SpreadsheetResources.SPREADSHEET_STRING);
        for (int rowIndex = 0; rowIndex < model.getSpreadSheetRowsNumber(); rowIndex++) {
            output("");
            for (int colIndex = 0; colIndex < model.getSpreadSheetColumnsNumber(); colIndex++) {
                if (model.getCellByIndex(rowIndex, colIndex).isError()) {
                    isErrorHappen = true;
                }
                System.out.format("%8s\t", model.getCellByIndex(rowIndex, colIndex).getValue());
            }
        }
        output("");
        output(SpreadsheetResources.END_STRING);
        if (isErrorHappen) {
            output(SpreadsheetResources.ERRORS_STRING);
            for (int rowIndex = 0; rowIndex < model.getSpreadSheetRowsNumber(); rowIndex++)
                for (int colIndex = 0; colIndex < model.getSpreadSheetColumnsNumber(); colIndex++) {
                    if (model.getCellByIndex(rowIndex, colIndex).isError()) {
                        System.out.format("For cell [%s%s] error is: %s\n", Character.toString((char)(colIndex + SpreadsheetResources.LITERA_A)), Integer.toString(rowIndex + 1), model.getCellByIndex(rowIndex, colIndex).getErrorMessage());
                    }
                }
        }
    }
}
