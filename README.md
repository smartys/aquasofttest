# README #

Spreadsheet simulator (Aquasoft test task)

### Project brief description? ###

Spreadsheet simulator allows to enter data through console or file.
In case of entering with a file, path to the file should be set as a line command parameter.

Spreadsheet simulator is designed in MVP pattern bounds:
o Model - spreadsheet;
o View - console;
o Presenter - communicates with model and user

Simulator works corresponding to requirments with few changes:
o Operations has priority and ability to increasing of operations amount;
o Cell with empty value is evaluated as 0 in case of number evaluation;
o If some errors happened in process of evaluating, they are printed in the end of the program output.